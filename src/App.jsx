import './App.css'
import Exercice1 from './Exo_01_Component/components/Exercice1'
import Exercice2 from './Exo_02_Conditions/Exercice2'
import Exercice3 from './Exo_03_Collections/Exercice3'
import Exercice4 from './Exo_04_State/Exercice4'
import Exercice5 from './Exo_05_Calculatrice/Exercice5'
import Exercice6 from './Exo_06_Presentation/Exercice6'
import Exercice6Bis from './Exo_06_Presentation/Exercice6Bis'
import Exercice7 from './Exo_07_ToDoList/Exercice7'
import Exercice8 from './Exo_08_Horloge/Exercice8'
import Exercice9 from './Exo_09_API_Meteo/Exercice9'


// const App = () => {}
function App() {


  return (
    <>
      {/* Exercice 1 - Composant et CSS */}
      {/* <Exercice1 nom="Georges" age={43} />
      <Exercice1 nom="Jean-Luc" />
      <Exercice1 nom={43} age="Oui" /> */}

      {/* Exercice 2 - Rendu conditionnel */}
      {/* <Exercice2 havePet={false} />
      <Exercice2 havePet={true} type="chien" />
      <Exercice2 havePet={true} type="chat" />
      <Exercice2 havePet={true} type="chat" name="Salami" /> */}

      {/* Exercice 3 - Rendu de collections */}
      {/* <Exercice3 /> */}

       {/* Exercice 4 - Le state */}
      {/* <Exercice4 increment={2} /> */}
      {/* <Exercice4 increment={6} /> */}
      {/* <Exercice4 /> */}

      {/* Exercice 5 - Formulaires -> Calculatrice */}
      {/* <Exercice5 /> */}

      {/* Exercice 6 - Formulaires -> Présentation */}
      {/* <Exercice6 /> */}

      {/* Exercice 6Bis - Formulaires -> Présentation avec React Hook Form */}
      {/* <Exercice6Bis /> */}

      {/* Exercice 7 - ToDoList*/}
      {/* <Exercice7 /> */}

      {/* Exercice 8 - Horloge*/}
      {/* <Exercice8 /> */}

      {/* Exercice 9 - API - Météo */}
      <Exercice9 />


    </>
  )
}

export default App

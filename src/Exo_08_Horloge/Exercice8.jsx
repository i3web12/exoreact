import React, { useState } from 'react'
import "./Horloge.css"
import Horloge from './Horloge'
import DateDuJour from './DateDuJour'

const Exercice8 = () => {
    const [ displayH, setDisplayH ] = useState(false)
  
    const toggleH = () => {
        setDisplayH(previousDisplay => !previousDisplay)
    }

  return (
    <div>
        <h2>Exercice Horloge - UseEffect() :</h2>
        <div>
            <button onClick={toggleH} className='btn'>Afficher Heure / Afficher Date</button>
        </div>
        { displayH ? 
            <Horloge />
            : 
            <DateDuJour />
        }


    </div>
  )
}

export default Exercice8
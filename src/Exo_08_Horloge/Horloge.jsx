import React, { useEffect, useState } from 'react'

const Horloge = () => {
  const [ time, setTime ] = useState(new Date());
  
  useEffect(() => {
      console.log("Lancement du timer !");
      const timer = setInterval(() => {
          setTime(new Date())
          console.log("TIC TAC");
        }, 500)

        //Nettoyage : Le composant en disparaissant va arrêter le timer
        return () => {
            console.log("Arrêt du timer !");
            clearInterval(timer)
        }

  }, [] /* Pour ne lancer l'interval qu'au lancement de la page */)

// ------- Avec un setTimeOut, on lance un TimeOut à l'apparition du composant, celui ci va modifier time, qui va rappeler le useEffect qui relancer un timeOut etc etc
//   useEffect(() => {

//     console.log("Lancement d'un timeout !");
//       const timer = setTimeout(() => {
//           setTime(new Date())
//           console.log("TIC TAC");
//     }, 500)

//     return () => {
//         console.log("Arrêt du timeout !");
//         clearTimeout(timer)
//     }
// }, [time])

  return (
    <div> { time.toLocaleTimeString("fr") } </div>
  )
}

export default Horloge
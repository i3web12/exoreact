import React, { useEffect, useState } from 'react'

const DateDuJour = () => {
    const [ date, setDate ] = useState(new Date())

    const options = {
        day :"2-digit", month : "long", year : "numeric"
    }

    useEffect(() => {
        //     -> 6/2/2024 -> 00h00
        // -
        // date -> 5/2/2024 -> 14h21
        // = temps avant de mettre à jour le composant
        
        //On construit une date au lendemain de la date générée à l'apparition du composant à minuit pile
        const dateRefresh = new Date( date.getFullYear() , date.getMonth() , date.getDate() + 1 ,0, 0, 0) ;

        //Pour le test
        // const dateRefresh = new Date( date.getFullYear() , date.getMonth() , date.getDate() ,14, date.getMinutes() + 1, 0) ;

        //Calcul du nombre de ms entre les deux dates
        const timeRefresh = dateRefresh.getTime() - date.getTime();
        console.log(timeRefresh);

        const timer = setTimeout(() => {
            console.log("Mise à jour de la date");
            setDate(new Date());
        }, timeRefresh ) //On rafraichit la date du jour à minuit tous les jours

        return () => {
            clearTimeout(timer)
        }

    }, [date])

  return (
    <div> { date.toLocaleDateString("fr", options) } </div>
  )
}

export default DateDuJour
import axios from "axios";
import { useEffect, useState } from "react";


const Meteo = (props) => {

    const { city } = props;

    const [ weather, setWeather ] = useState({ icon : "", desc : "", temp : 0, tempMin : 0, tempMax : 0 });

    useEffect( () => {
        const apiKey = "4123bf9e94cd4b058f791b7bf2b5a75c";
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=fr&units=metric&appid=${apiKey}`;

        // ! Attention à ne lancer la requête que si city est rempli
        if(city) {
            // apiRequest();
            axios.get(url)
                .then((res) => {
                    console.log(res.data);
                    setWeather({ icon : res.data.weather[0].icon , desc : res.data.weather[0].description, temp : res.data.main.temp, tempMin : res.data.main.temp_min, tempMax : res.data.main.temp_max})
                })
                .catch((error) => {
                    console.log(error);
                })
                .finally(() => {
    
                })
        }


    }, [city]) //! Attention à relancer le useEffect quand city change

    // Si on veut utiliser le async/await
    const apiRequest = async() => {
        const apiKey = "4123bf9e94cd4b058f791b7bf2b5a75c";
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=fr&units=metric&appid=${apiKey}`;

        try {
            const res = await axios.get(url);
            setWeather({ icon : res.data.weather[0].icon , desc : res.data.weather[0].description, temp : res.data.main.temp, tempMin : res.data.main.temp_min, tempMax : res.data.main.temp_max});

        }
        catch(error) {
            console.log(error);
        }                
         //finally      
    }

    if(!city) {
        return (
            <>
                <p>Veuillez entrer une ville dans la barre de recherche</p>
            </>
        )
    }
  
    return (
        <>
            <div>Meteo de la ville de { city }</div>
            <div>
                <div>
                    <img src={ `https://openweathermap.org/img/wn/${weather.icon}@2x.png` } alt={ 'météo de la ville de ' + city } />
                    <p>{ weather.desc }</p>
                </div>
                <div>
                    <p>Température actuelle : { Math.round(weather.temp)}°C</p>
                    <p>Température min : {Math.round(weather.tempMin)}°C</p>
                    <p>Température max : {Math.round(weather.tempMax)}°C</p>

                </div>
            </div>
        </>
  )
}

export default Meteo
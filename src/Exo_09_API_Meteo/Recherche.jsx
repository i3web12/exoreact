import { useState } from "react"

const Recherche = (props) => {
    const { onSearch } = props;


    const [city, setCity] = useState("");

    const sendCity = (e) => {
        e.preventDefault();
        onSearch(city); //On déclenche l'évènement onSearch en envoyant la ville pour que le parent puisse réagir à l'évènement et traiter la ville
        setCity("");

    }
 
  return (
    <div>
        <form onSubmit={ sendCity } >
            <input type="text" placeholder="Entrez une ville" value={city} onChange={ (e) => setCity(e.target.value)  } />
            <input type="submit" value="Rechercher" />
        </form>
    </div>
  )
}

export default Recherche
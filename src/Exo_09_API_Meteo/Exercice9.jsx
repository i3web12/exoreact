import { useState } from "react"
import Meteo from "./Meteo"
import Recherche from "./Recherche"


const Exercice9 = () => {

    const [ cityToSend, setCityToSend ] = useState("");

    const search = (city) => {
        setCityToSend(city);
        
    }

  return (
    <>
        <h2>Application Météo</h2>
        <Recherche onSearch={ search } />
        <Meteo city={cityToSend} />
    </>
  )
}

export default Exercice9
import { useForm } from "react-hook-form";
import "./Exercice7.css"
import { useState } from "react";
import Task from "./Task";

// npm i react-hook-form


const Exercice7 = () => {
    const { register, handleSubmit, formState : { errors, isValid }, reset, setFocus } = useForm({ mode : "onBlur"})

    const [filtre, setFiltre] = useState("")

    const [taskList, setTaskList] = useState([
        { id : 1, name : "Faire la vaisselle", description : "Ca déborde là", priority : "high", complete : false },
        { id : 2, name : "Faire un bolo pour 12", description : "Y'a toute la famille qui vient", priority : "low", complete : false },
        { id : 3, name : "Etendre le linge", description : "Sinon ça va puer", priority : "high", complete : true },
    ])

    const addTask = (data) => {
        console.log(data);

        //taskList.map(t =>  t.id ) -> Renvoie un tableau avec juste les id [1, 2, 3]
        //...taskList.map(t =>  t.id ) -> Renvoie 1, 2, 3
        //Math.max(...taskList.map(t =>  t.id )) -> Math.max(1, 2, 3) -> 3
        let currentIndex = Math.max(...taskList.map(t =>  t.id ));

        //Si taskList est vide on met l'id à 1, sinon on prend le max déjà présent + 1
        setTaskList([...taskList, {...data, id : taskList.length == 0 ? 1 : currentIndex + 1, complete : false }])
        reset() //vient de useForm et qui permet de remettre à 0 le formulaire
    }

    const completeTask = (task) => {

        //On modifie la liste pour qu'elle soit égale à elle même mais avec un élément modifié
        // si t.id (t -> élément qu'on est en train de parcourir) est égal à task.id (task -> élément reçu en paramètre), on change le complete et on renvoie t
        // si les id ne correspondent pas, on renvoie t sans modifier
        setTaskList([...taskList.map(t => {
            if(t.id === task.id) {
                t.complete = true;
                return t;
            } else {
                return t;
            }
        })])
    }

    const deleteTask = (task) => {
        //On remplace la liste par la liste filtrée : On veut toutes les tâches dont l'id n'est pas l'id de la tâche reçu en paramètre (celle sur laquelle on a cliqué)
        setTaskList([...taskList.filter(t => t.id !== task.id)])

    }


    return (
        <>
            <h2>Exercice ToDoList :</h2>
            <div className="task-exo">

                <div className="add-task">
                    <h3>Ajouter une nouvelle tâche :</h3>
                    <form onSubmit={handleSubmit(addTask)}>
                        <div>
                            <label htmlFor="name">Nom :</label>
                            <input id="name" type="text" {...register("name", { required : true })}  />
                            { errors.name && <span className="error">Ce champs est requis</span>  }
                            {/* { errors.name?.type === "required" && <span className="error">Ce champs est requis</span>  } */}
                        </div>
                        <div>
                            <label htmlFor="description">Description :</label>
                            <textarea id="description" {...register("description")} />
                        </div>
                        <div>
                            <label htmlFor="priority">Priorité</label>
                            <select id="priority" {...register("priority")}>
                                <option value="normal">Normale</option>
                                <option value="low">Basse</option>
                                <option value="high">Urgente</option>
                            </select>
                        </div>
                        <div>
                            <button disabled={!isValid}>Ajouter</button>
                        </div>
                    </form>

                </div>

                <div className="task-list">
                    <h3>Liste des tâches :</h3>
                    <select value={filtre} onChange={ (e) => setFiltre(e.target.value) }>
                        <option value="">Tous</option>
                        <option value="normal">Normale</option>
                        <option value="low">Basse</option>
                        <option value="high">Urgente</option>
                    </select>
                    <div className="tasks">
                        {
                            taskList.filter(t => { 
                                if(filtre !== "") {
                                    return t.priority === filtre
                                }
                                else {
                                    return t
                                }
                            }).map(t => <Task key={t.id} task={t} onComplete={completeTask} onDelete={deleteTask} />)
                        }
                    </div>
                </div>

            </div>
        </>
    )
}

export default Exercice7;
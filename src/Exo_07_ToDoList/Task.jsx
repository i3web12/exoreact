
const Task = (props) => {
    const { task, onComplete, onDelete } = props;

    return (
        <>
            <div className={ (task.complete) ? "task-card complete" : (task.priority === "high") ? "task-card urgent" : "task-card"}>
                <div className="task-infos">
                    <h4>{task.name}</h4>
                    <p>{task.description}</p>
                </div>
                <div className="task-actions">
                    <button onClick={() => { onComplete(task) }} disabled={task.complete}>Terminer</button>
                    <button onClick={() => { onDelete(task) }}>Supprimer</button>
                </div>
            </div> 
        </>
    )

}

export default Task;